<!DOCTYPE html>
<html>
<head>
        <title>Php calculator</title>
</head>
<body>

<?php
        $firstnum = $_GET['firstnum'];
        $secondnum = $_GET['secondnum'];
        $operation = $_GET['operation'];
	
	if ((float)$secondnum == 0 And $operation=="divide"){
		echo "cannot divide by 0, sorry";
		exit(1);
	}
		
	switch($operation) {
		case "add": $answer = $firstnum + $secondnum; $opp = "+"; break;
		case "subtract": $answer = $firstnum - $secondnum; $opp = "-"; break;
		case "multiply": $answer = $firstnum * $secondnum; $opp = "*"; break;
		case "divide": $answer = $firstnum / $secondnum; $opp = "/"; break;
		break;
	}

        echo "<p>";
	echo (float)htmlentities($firstnum), " $opp ", (float)htmlentities($secondnum), " = ", (float)$answer;
	echo "</p>";
	
?>

</body>
</html>
